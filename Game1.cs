﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Minesweep
{

    public class Game1 : Game
    {
        public static GraphicsDeviceManager _graphics;
        public static SpriteBatch _spriteBatch;
        public static SpriteFont menuText;
        public static ContentManager content;
        float width, height;
        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            height = _graphics.PreferredBackBufferHeight;
            width = _graphics.PreferredBackBufferWidth;
            Content.RootDirectory = "Content";
            content = Content;
            this.IsMouseVisible = true;
        }

        public static float scaleX; 
        public static float scaleY; 
        Vector3 _screenScale;
        public int VirtualScreenWidth = 1024;
        public int VirtualScreenHeight = 768;
        MouseState previousMouseState;
        KeyboardState oldState;

        Puzzle puzzle = new Puzzle();

        protected override void Initialize()
        {
            base.Initialize();
            previousMouseState = Mouse.GetState();

            scaleX = (float)GraphicsDevice.Viewport.Width / (float)VirtualScreenWidth;;
            scaleY = (float)GraphicsDevice.Viewport.Height / (float)VirtualScreenHeight;
            _screenScale = new Vector3(scaleX, scaleY, 1.0f);

            puzzle.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            menuText = Content.Load<SpriteFont>("console");
            puzzle.LoadContent();
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            var mouseState = Mouse.GetState(); //not used
            var keyboardState = Keyboard.GetState(); //not used
            KeyboardState newState = Keyboard.GetState();  // get the newest state

            previousMouseState = Mouse.GetState(); //not used

            puzzle.Update(gameTime); //Main code is in the puzzle class
            base.Update(gameTime);
        }
        
        protected override void Draw(GameTime gameTime)
        {
            var scaleMatrix = Matrix.CreateScale(_screenScale);
            GraphicsDevice.Clear(Color.Gray);

            //game was created in the lowest required resolution, this will scale it up if user has a larger resolution
            _spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, scaleMatrix); 

            puzzle.Draw(_spriteBatch, gameTime);
            _spriteBatch.End();
            base.Draw(gameTime);
        }

 
    }
}
