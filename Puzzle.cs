﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.UI.Xaml.Shapes;

namespace Minesweep
{
    public class Piece
    {

        public Texture2D texture;
        public int id;

        public Piece(Texture2D Texture, int Id)
        {
            texture = Texture;
            id = Id;
        }
    }


    public class Puzzle
    {
        Texture2D puzzle, puzzle_icon, layout, arrow, start_button, option_button, box, box_outline, box_outlineshadow, background, selectpuzzle_box;
        Texture2D newPuzzle, returnPuzzle, box_16, box_64, box_256, box_back, scrollmode, tablemode;

        Color[] puzzleColor, layoutColor, transfer_Color;
        MouseState previousMouseState;
        Vector2[] fp_pos;

        SpriteFont menuText;
        List<Piece>[] final_piece;
        int puzzlePick = 0;
        int menuPick = 0;
        int pieceCount;
        int rowCount;
        Texture2D[] puzzlePreview = new Texture2D[64];
        Windows.Storage.ApplicationDataContainer localSettings;
        int piece_clicked = -1;
        Color tint;
        bool drawPuzzle = false;
        int piece_width;
        String layoutname;
        public static SoundEffect cardhit;
        public static SoundEffectInstance hitInstance;
        int mx, my;
        int piece1count;
        int piece2count;
        bool drawPuzzleSelect = true;
        bool return_to_puzzle = false;
        bool options = false;
        bool slidebar = true;
        bool puzzleload = false;
        float bottomoffset; 
        public void Initialize()
        {
            //Layouts determine how big the puzzle will be. Small medium or large? 
            //Information stored in local settings and remembered on app reopen.
            localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            localSettings.Values["puzzleSize"] = "layout_3";
            localSettings.Values["mode"] = "table";
        }

        public void LoadContent()
        {
            //Loading the textures for the puzzle game.
            tablemode = Game1.content.Load<Texture2D>("tablemode");
            scrollmode = Game1.content.Load<Texture2D>("scrollmode");
            box_back = Game1.content.Load<Texture2D>("box_back");
            box_16 = Game1.content.Load<Texture2D>("box_16");
            box_64 = Game1.content.Load<Texture2D>("box_64");
            box_256 = Game1.content.Load<Texture2D>("box_256");
            newPuzzle = Game1.content.Load<Texture2D>("new_puzzle");
            returnPuzzle = Game1.content.Load<Texture2D>("return_button");
            start_button = Game1.content.Load<Texture2D>("start_button");
            option_button = Game1.content.Load<Texture2D>("option_button");
            puzzle_icon = Game1.content.Load<Texture2D>("puzzle_icon");
            box_outline = Game1.content.Load<Texture2D>("box_outline");
            box_outlineshadow = Game1.content.Load<Texture2D>("box_outlineshadow");
            selectpuzzle_box = Game1.content.Load<Texture2D>("selectPuzzle_box");
            background = Game1.content.Load<Texture2D>("background");
            menuText = Game1.content.Load<SpriteFont>("console");
            arrow = Game1.content.Load<Texture2D>("arrow_right");
            box = Game1.content.Load<Texture2D>("box");

            cardhit = Game1.content.Load<SoundEffect>("puzzlenoise");
            hitInstance = cardhit.CreateInstance();
            for (int i = 0; i < 64; i++)
            {
                puzzlePreview[i] = Game1.content.Load<Texture2D>("puzzle/random_" + (i + 1));
            }
        }

        public void LoadPuzzle()
        {
            //Layout loaded determines the puzzle size. 
            if (localSettings.Values["puzzleSize"].ToString() == "layout_3")
            {
                layoutname = "layout_3";
                piece_width = 32;
                pieceCount = 256;
                rowCount = 16;
            }
            else if (localSettings.Values["puzzleSize"].ToString() == "layout_2")
            {
                layoutname = "layout_2";
                piece_width = 64;
                pieceCount = 64;
                rowCount = 8;
            }
            else
            {
                layoutname = "layout_1";
                piece_width = 128;
                pieceCount = 16;
                rowCount = 4;
            }

            //Full image to be a puzzle
            puzzle = Game1.content.Load<Texture2D>("puzzle/random_" + (puzzlePick+1));
            puzzleColor = new Color[puzzle.Width * puzzle.Height];
            puzzle.GetData<Color>(puzzleColor);

            //layout image template
            layout = Game1.content.Load<Texture2D>(layoutname);
            layoutColor = new Color[layout.Width * layout.Height];
            layout.GetData<Color>(layoutColor);

            //Below begins the magic process. The template contains a picture of all the puzzle pieces. Each piece contains a unique 
            //blue hue value ranging from perfect blue to white. We match that blue value to the puzzle image.
            transfer_Color = new Color[layout.Width * layout.Height];
            final_piece = new List<Piece>[pieceCount];
            fp_pos = new Vector2[pieceCount];

            //top row pieces are slightly smaller and must be processed first
            for (int color = 0; color < rowCount ; color++)
            {
                int offset = color * piece_width - (int)(piece_width / 2f);
                if (offset < 0) offset = 0;
                int offsetEnd = offset + (int)(piece_width * 1.95f);
                for (int i = offset; i < offsetEnd; i++)
                {
                    for (int j = 0; j < (int)(piece_width * 2.10f); j++)
                    {
                        if (i + (j * layout.Width) < layout.Width * layout.Height)
                        {
                            if (layoutColor[i + (j * layout.Width)] == new Color(0f, 0f, (255f - color) / 255f, 1.0f))
                            {
                                transfer_Color[i - offset + (j * (offsetEnd - offset))] = puzzleColor[i + (j * layout.Width)];
                            }
                            else
                            {
                                transfer_Color[i - offset + (j * (offsetEnd - offset))] = Color.White * 0.0f;
                            }
                        }
                    }
                }
                final_piece[color] = new List<Piece>();
                final_piece[color].Add(new Piece(new Texture2D(Game1._graphics.GraphicsDevice, offsetEnd - offset, (int)(piece_width * 2.1f)), color));
                fp_pos[color] = Vector2.Zero;
                final_piece[color][0].texture.SetData<Color>(transfer_Color);
            }

            //remaining puzzle pieces not in the top row
            for (int color = rowCount; color < pieceCount; color++)
            {
                int offset = color % rowCount * piece_width - (int)(piece_width / 2f);
                if (offset < 0) offset = 0;
                int offsetEnd = offset + (int)(piece_width * 2.1f);

                for (int i = offset; i < offsetEnd; i++)
                {
                    for (int j = color / rowCount * piece_width - piece_width / 2; j < color / rowCount * piece_width + (int)(piece_width * 1.75f); j++)
                    {
                        if (i + (j * layout.Width) < layout.Width * layout.Height)
                        {
                            if (layoutColor[i + (j * layout.Width)] == new Color(0f, 0f, (255f - color) / 255f, 1.0f))
                            {
                                transfer_Color[i - offset + ((j - color / rowCount * piece_width + piece_width / 2 ) * (offsetEnd - offset))] = puzzleColor[i + (j * layout.Width)];
                            }
                            else
                            {
                                transfer_Color[i - offset + ((j - color / rowCount * piece_width + piece_width / 2) * (offsetEnd - offset))] = Color.Red * 0.0f;
                            }
                        }
                        else
                        {
                            transfer_Color[i - offset + ((j - color / rowCount * piece_width + piece_width / 2) * (offsetEnd - offset))] = Color.Red * 0.0f;
                        }
                    }
                }
                final_piece[color] = new List<Piece>();
                final_piece[color].Add(new Piece(new Texture2D(Game1._graphics.GraphicsDevice, offsetEnd - offset, (int)(piece_width * 2.5f)), color));
                fp_pos[color] = Vector2.Zero;
                final_piece[color][0].texture.SetData<Color>(transfer_Color);
            }

            //game has two modes, either all the pieces are on the table at one time, or they are placed into a slidebar that shows only a
            //few at a time.
            if (!slidebar)
            {
                for (int i = 0; i < pieceCount; i++)
                {
                    if (i % rowCount == 0 && i < rowCount)
                        fp_pos[i] = new Vector2(piece_width * 2 * (i % rowCount) + piece_width / 2, 25f + piece_width * 1.3f * (i / rowCount) + piece_width / 2);
                    else if (i < rowCount)
                        fp_pos[i] = new Vector2(piece_width * 2 * (i % rowCount), 25f + piece_width * 1.3f * (i / rowCount) + piece_width / 2);
                    else if (i % rowCount == 0)
                        fp_pos[i] = new Vector2(piece_width * 2 * (i % rowCount) + piece_width / 2, 25f + piece_width * 1.3f * (i / rowCount));
                    else
                        fp_pos[i] = new Vector2(piece_width * 2 * (i % rowCount), 25f + piece_width * 1.3f * (i / rowCount));
                }
            }

            if (slidebar)
            {
                bottomoffset = 768f - piece_width * 2.0f;
                for (int i = 0; i < pieceCount; i++)
                {
                    if (i % rowCount == 0 && i < rowCount)
                        fp_pos[i] = new Vector2(25f + i * piece_width * 1.8f + piece_width / 2, bottomoffset);
                    else if (i < rowCount)
                        fp_pos[i] = new Vector2(25f + i * piece_width * 1.8f, bottomoffset);
                    else if (i % rowCount == 0)
                        fp_pos[i] = new Vector2(25f + i * piece_width * 1.8f + piece_width / 2, bottomoffset);
                    else
                        fp_pos[i] = new Vector2(25f + i * piece_width * 1.8f, bottomoffset);
                    //fp_pos[i] = new Vector2(25f + i * piece_width * 1.8f, 768f - piece_width);
                }

                if (layoutname == "layout_1")
                {
                    for (int i = 0; i < pieceCount; i++)
                    {
                        if (i % rowCount == 0 && i < rowCount)
                            fp_pos[i] = new Vector2(25f + i * piece_width * 1.8f + piece_width / 4, bottomoffset);
                        else if (i < rowCount)
                            fp_pos[i] = new Vector2(25f + i * piece_width * 1.8f, bottomoffset);
                        else if (i % rowCount == 0)
                            fp_pos[i] = new Vector2(25f + i * piece_width * 1.8f + piece_width / 4, bottomoffset);
                        else
                            fp_pos[i] = new Vector2(25f + i * piece_width * 1.8f, bottomoffset);
                    }
                }
            }
            ShuffleArray(fp_pos);
            puzzleload = true;
            slidebarNum = 0;
        }

        public void Update(GameTime gameTime)
        {
            var mouseState = Mouse.GetState();

            mx = (int)(mouseState.X / Game1.scaleX);
            my = (int)(mouseState.Y / Game1.scaleY);

            
            if (previousMouseState.LeftButton == ButtonState.Released && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                if (mx >= 0 && mx < 204 && my >= 0 && my < 33 && drawPuzzle)
                {
                    drawPuzzleSelect = true;
                    drawPuzzle = false;
                }

                if (puzzle != null)
                {
                    return_to_puzzle = true;
                }

                if (options) //option menu is open
                {
                    if (mx >= 850 && mx < 850 + 100 && my >= 690 && my < 690 + 67)
                        options = !options;
                    else if (mx >= 125 && mx < 125 + 231 && my >= 150 && my < 150 + 233)
                        localSettings.Values["puzzleSize"] = "layout_1";
                    else if (mx >= 400 && mx < 400 + 231 && my >= 150 && my < 150 + 233)
                        localSettings.Values["puzzleSize"] = "layout_3";
                    else if (mx >= 125 && mx < 125 + 231 && my >= 400 && my < 400 + 233)
                        localSettings.Values["puzzleSize"] = "layout_2";
                    else if (mx >= 675 && mx < 675 + 231 && my >= 150 && my < 150 + 233)
                    {
                        localSettings.Values["mode"] = "scroll";
                        slidebar = true;
                    }
                    else if (mx >= 675 && mx < 675 + 231 && my >= 400 && my < 400 + 233)
                    {
                        localSettings.Values["mode"] = "table";
                        slidebar = false;
                    }

                }

                else if (drawPuzzleSelect)
                    SetMenuPuzzlePick(mx, my);
            }

            

            if (drawPuzzle)
            {
                if (piece_clicked == -1 && !puzzleload)
                {
                    if (previousMouseState.LeftButton == ButtonState.Released && Mouse.GetState().LeftButton == ButtonState.Pressed)
                    {
                        for (int i = 0; i < pieceCount; i++)
                        {
                            if (final_piece[i].Count > 0)
                            {
                                if (mx > fp_pos[i].X && mx < fp_pos[i].X + piece_width * 1.3f)
                                    if (my > fp_pos[i].Y && my < fp_pos[i].Y + piece_width * 1.3f)
                                        piece_clicked = i;
                            }
                        }
                    }
                }

                else if (piece_clicked >= 0)
                {
                    Vector2 dif = Vector2.Zero;
                    for (int i = 0; i < final_piece[piece_clicked].Count; i++)
                    {
                        if (i == 0)
                        {
                            dif = fp_pos[final_piece[piece_clicked][i].id] - new Vector2(mx - piece_width / 2, my - piece_width / 2);
                            fp_pos[final_piece[piece_clicked][i].id] -= dif;
                        }
                        else
                            fp_pos[final_piece[piece_clicked][i].id] -= dif;
                    }
                }

                if (previousMouseState.LeftButton == ButtonState.Released && Mouse.GetState().LeftButton == ButtonState.Released)
                {
                    puzzleload = false;
                    PuzzlePieceDropped();
                }

                if (slidebar)
                {
                    if (previousMouseState.RightButton == ButtonState.Released && Mouse.GetState().RightButton == ButtonState.Pressed)
                    {
                        slidebarNum = (slidebarNum + 1) % rowCount;
                        for (int i = 0; i < pieceCount; i++)
                        {
                            if (slidebarNum == 0 && fp_pos[i].Y == bottomoffset)
                                fp_pos[i].X += piece_width * 1.8f * rowCount * (rowCount-1);
                            else if (fp_pos[i].Y == bottomoffset)
                                fp_pos[i].X -= piece_width * 1.8f * rowCount;
                        }
                    }
                }

            }
            previousMouseState = Mouse.GetState();
        }

        int slidebarNum = 0;
        public void Draw(SpriteBatch _spriteBatch, GameTime gameTime)
        {
            _spriteBatch.Draw(background, new Vector2(0, 0), null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.FlipHorizontally, 0.0f);

            if (drawPuzzleSelect) //draw a screen where the user selects from all the puzzles
            {
                _spriteBatch.Draw(option_button, new Vector2(850, 690), null, Color.White, 0.0f, Vector2.Zero, 0.7f, SpriteEffects.None, 0.0f);


                if (!options) 
                {
                    if (return_to_puzzle)
                        _spriteBatch.Draw(returnPuzzle, new Vector2(25, 680), null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);

                    _spriteBatch.Draw(selectpuzzle_box, new Vector2(130, 0), null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
                    _spriteBatch.Draw(start_button, new Vector2(725, 690), null, Color.White, 0.0f, Vector2.Zero, 0.7f, SpriteEffects.None, 0.0f);

                    DrawPuzzlePreview(_spriteBatch);

                    for (int i = 0; i < 8; i++)
                    {
                        if (i == menuPick)
                            _spriteBatch.Draw(puzzle_icon, new Vector2(325 + 50 * i, 690), null, Color.Green, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
                        else
                            _spriteBatch.Draw(puzzle_icon, new Vector2(325 + 50 * i, 690), null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
                    }
                }
                else
                    DrawPuzzleSizeBox(_spriteBatch);
            }

            if (drawPuzzle)
            {
                if (final_piece[0].Count != pieceCount)
                {
                    if (slidebar)
                    {
                        for (int i = 0; i < pieceCount; i++)
                        {
                            if (fp_pos[i].X > 25f && fp_pos[i].X <= 25f + piece_width * 1.8f * rowCount)
                                _spriteBatch.Draw(final_piece[i][0].texture, fp_pos[i], null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);


                            else if (fp_pos[i].Y < bottomoffset)
                            {
                                _spriteBatch.Draw(final_piece[i][0].texture, fp_pos[i], null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);
                            }
                        }
                    }

                    else
                    {
                        for (int i = 0; i < pieceCount; i++)
                        {
                            _spriteBatch.Draw(final_piece[i][0].texture, fp_pos[final_piece[i][0].id], null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);
                        }
                    }
                    _spriteBatch.Draw(newPuzzle, new Vector2(0,0), null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);

                }
                else
                {
                    Victory(_spriteBatch);
                }
            }

        }

        public void PuzzlePieceDropped() //user had picked up the puzzle piece but now let go of it
        {
            if (piece_clicked != -1)
            {
                hitInstance.Stop();
                hitInstance.Play(); //clicky sound to signify the piece was dropped

                //look if piece to left is not already connected, if not connected then check if it is in the position to connect
                if (piece_clicked - 1 >= 0 && !final_piece[piece_clicked].Contains(final_piece[piece_clicked - 1][0]) && fp_pos[piece_clicked].X >= fp_pos[piece_clicked - 1].X + piece_width * 0.45f && fp_pos[piece_clicked].X <= fp_pos[piece_clicked - 1].X + piece_width * 1.1f && fp_pos[piece_clicked].Y >= fp_pos[piece_clicked - 1].Y - piece_width * 0.35f && fp_pos[piece_clicked].Y <= fp_pos[piece_clicked - 1].Y + piece_width * 0.35f)
                    CheckPiecetoLeft(); //pieces may need to move together to form a proper snap, user may of been 'close enough' to match 

                else if (piece_clicked + 1 < pieceCount && !final_piece[piece_clicked].Contains(final_piece[piece_clicked + 1][0]) && fp_pos[piece_clicked].X <= fp_pos[piece_clicked + 1].X - piece_width * 0.45f && fp_pos[piece_clicked].X >= fp_pos[piece_clicked + 1].X - piece_width * 1.15f && fp_pos[piece_clicked].Y >= fp_pos[piece_clicked + 1].Y - piece_width * 0.35f && fp_pos[piece_clicked].Y <= fp_pos[piece_clicked + 1].Y + piece_width * 0.35f)
                    CheckPiecetoRight();

                else if (piece_clicked + rowCount < pieceCount && !final_piece[piece_clicked].Contains(final_piece[piece_clicked + rowCount][0]) && fp_pos[piece_clicked].X >= fp_pos[piece_clicked + rowCount].X - piece_width * 0.15f && fp_pos[piece_clicked].X <= fp_pos[piece_clicked + rowCount].X + piece_width * 1.15f && fp_pos[piece_clicked].Y <= fp_pos[piece_clicked + rowCount].Y && fp_pos[piece_clicked].Y >= fp_pos[piece_clicked + rowCount].Y - piece_width * 1.15f)
                    CheckPieceBelow();

                else if (piece_clicked - rowCount >= 0 && !final_piece[piece_clicked].Contains(final_piece[piece_clicked - rowCount][0]) && fp_pos[piece_clicked].X >= fp_pos[piece_clicked - rowCount].X - piece_width * 0.15f && fp_pos[piece_clicked].X <= fp_pos[piece_clicked - rowCount].X + piece_width * 1.15f && fp_pos[piece_clicked].Y >= fp_pos[piece_clicked - rowCount].Y && fp_pos[piece_clicked].Y <= fp_pos[piece_clicked - rowCount].Y + piece_width * 1.15f)
                    CheckPieceAbove();
            }
            piece_clicked = -1;
        }

        public int[] ShuffleIntArray(int[] array)
        {   //shuffle the puzzle pieces
            Random r = new Random();
            for (int i = array.Length; i > 0; i--)
            {
                int j = r.Next(i);
                int k = array[j];
                array[j] = array[i - 1];
                array[i - 1] = k;
            }
            return array;
        }

        public Vector2[] ShuffleArray(Vector2[] array)
        {
            //shuffle the puzzle pieces x y positions
            Random r = new Random();
            for (int i = array.Length; i > 0; i--)
            {
                int j = r.Next(i);
                Vector2 k = array[j];
                array[j] = array[i - 1];
                array[i - 1] = k;
            }
            return array;
        }

        public void Victory(SpriteBatch _spriteBatch)
        {
            //You won! You get a well done message !
            _spriteBatch.Draw(puzzle, new Vector2(100, 100), null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
            _spriteBatch.DrawString(menuText, "Well Done!", new Vector2(280, 620), Color.White);
            _spriteBatch.Draw(newPuzzle, new Vector2(0, 0), null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);
        }

        public void SetMenuPuzzlePick(int mx, int my)
        {
            //Select the on screen puzzle pieces to cycle through more images on the puzzle select screen.
            if (mx >= 325 && mx < 361 && my >= 690 && my < 736)
                menuPick = 0;
            else if (mx >= 325 + 50 && mx < 361 + 50 && my >= 690 && my < 736)
                menuPick = 1;
            else if (mx >= 325 + 50 * 2 && mx < 361 + 50 * 2 && my >= 690 && my < 736)
                menuPick = 2;
            else if (mx >= 325 + 50 * 3 && mx < 361 + 50 * 3 && my >= 690 && my < 736)
                menuPick = 3;
            else if (mx >= 325 + 50 * 4 && mx < 361 + 50 * 4 && my >= 690 && my < 736)
                menuPick = 4;
            else if (mx >= 325 + 50 * 5 && mx < 361 + 50 * 5 && my >= 690 && my < 736)
                menuPick = 5;
            else if (mx >= 325 + 50 * 6 && mx < 361 + 50 * 6 && my >= 690 && my < 736)
                menuPick = 6;
            else if (mx >= 325 + 50 * 7 && mx < 361 + 50 * 7 && my >= 690 && my < 736)
                menuPick = 7;

            else if (mx >= 725 && mx < 725 + 100 && my >= 690 && my < 690 + 67 && !options)
            {
                puzzlePick = puzzlePick + 8 * menuPick;
                LoadPuzzle();
                puzzlePick = puzzlePick - 8 * menuPick;
                drawPuzzle = true;
                drawPuzzleSelect = false;
            }
            else if (mx >= 850 && mx < 850 + 100 && my >= 690 && my < 690 + 67)
                options = !options;

            else if (mx >= 25 && mx < 25 + 230 && my >= 680 && my < 680 + 67 && return_to_puzzle)
            {
                drawPuzzle = true;
                drawPuzzleSelect = false;
            }

            //Selecting the actual puzzle!
            else if (mx >= 25 && mx < 256 && my >= 125 && my < 358)
                puzzlePick = 0;

            else if (mx >= 270 && mx < 501 && my >= 125 && my < 358)
                puzzlePick = 1;

            else if (mx >= 515 && mx < 746 && my >= 125 && my < 358)
                puzzlePick = 2;

            else if (mx >= 760 && mx < 991 && my >= 125 && my < 358)
                puzzlePick = 3;

            else if (mx >= 25 && mx < 256 && my >= 425 && my < 658)
                puzzlePick = 4;

            else if (mx >= 270 && mx < 501 && my >= 425 && my < 658)
                puzzlePick = 5;

            else if (mx >= 515 && mx < 746 && my >= 425 && my < 658)
                puzzlePick = 6;

            else if (mx >= 760 && mx < 991 && my >= 425 && my < 658)
                puzzlePick = 7;
        }

        public void CheckPiecetoLeft()
        {
            //Move the pieces together to a perfect fit. The user may of been close enough that we will automate them together.
            //Each piece will add each other to their list of connected pieces 
            if (!final_piece[piece_clicked].Contains(final_piece[piece_clicked - 1][0]))
            {
                Vector2 dif = Vector2.Zero;

                for (int i = 0; i < final_piece[piece_clicked].Count; i++)
                {
                    if (i == 0)
                    {
                        dif.X = fp_pos[final_piece[piece_clicked][0].id].X - fp_pos[final_piece[piece_clicked - 1][0].id].X - piece_width * 1.005f;
                        dif.Y = fp_pos[final_piece[piece_clicked][0].id].Y - fp_pos[final_piece[piece_clicked - 1][0].id].Y;
                        if ((piece_clicked - 1) % rowCount == 0)
                            dif.X = fp_pos[final_piece[piece_clicked][0].id].X - fp_pos[final_piece[piece_clicked - 1][0].id].X - piece_width * 0.501f;

                        fp_pos[final_piece[piece_clicked][0].id] -= dif;
                    }
                    else
                        fp_pos[final_piece[piece_clicked][i].id] -= dif;
                }

                piece1count = final_piece[piece_clicked - 1].Count;
                piece2count = final_piece[piece_clicked].Count;

                for (int i = 0; i < piece1count; i++)
                {
                    for (int j = 0; j < piece2count; j++)
                    {
                        if (!final_piece[final_piece[piece_clicked - 1][i].id].Contains(final_piece[piece_clicked][j]))
                        {
                            final_piece[final_piece[piece_clicked - 1][i].id].Add(final_piece[piece_clicked][j]);
                            final_piece[final_piece[piece_clicked][j].id].Add(final_piece[final_piece[piece_clicked - 1][i].id][0]);
                        }
                    }
                }
            }
        }

        public void CheckPiecetoRight()
        {
            //Move the pieces together to a perfect fit. The user may of been close enough that we will automate them together.
            //Each piece will add each other to their list of connected pieces 
            if (!final_piece[piece_clicked].Contains(final_piece[piece_clicked + 1][0]))
            {
                Vector2 dif = Vector2.Zero;

                for (int i = 0; i < final_piece[piece_clicked].Count; i++)
                {
                    if (i == 0)
                    {
                        dif.X = fp_pos[final_piece[piece_clicked][0].id].X - fp_pos[final_piece[piece_clicked + 1][0].id].X + piece_width * 1.005f;
                        dif.Y = fp_pos[final_piece[piece_clicked][0].id].Y - fp_pos[final_piece[piece_clicked + 1][0].id].Y;
                        if (piece_clicked % rowCount == 0)
                            dif.X = fp_pos[final_piece[piece_clicked][0].id].X - fp_pos[final_piece[piece_clicked + 1][0].id].X + piece_width * 0.501f;

                        fp_pos[final_piece[piece_clicked][i].id] -= dif;
                    }
                    else
                        fp_pos[final_piece[piece_clicked][i].id] -= dif;
                }

                piece1count = final_piece[piece_clicked + 1].Count;
                piece2count = final_piece[piece_clicked].Count;

                for (int i = 0; i < piece1count; i++)
                {
                    for (int j = 0; j < piece2count; j++)
                    {
                        if (!final_piece[final_piece[piece_clicked + 1][i].id].Contains(final_piece[piece_clicked][j]))
                        {
                            final_piece[final_piece[piece_clicked + 1][i].id].Add(final_piece[piece_clicked][j]);
                            final_piece[final_piece[piece_clicked][j].id].Add(final_piece[final_piece[piece_clicked + 1][i].id][0]);
                        }
                    }
                }

            }
        }

        public void CheckPieceAbove()
        {
            //Move the pieces together to a perfect fit. The user may of been close enough that we will automate them together.
            //Each piece will add each other to their list of connected pieces 
            if (!final_piece[piece_clicked].Contains(final_piece[piece_clicked - rowCount][0]))
            {
                Vector2 dif = Vector2.Zero;

                for (int i = 0; i < final_piece[piece_clicked].Count; i++)
                {
                    if (i == 0)
                    {
                        dif.X = fp_pos[final_piece[piece_clicked][0].id].X - fp_pos[final_piece[piece_clicked - rowCount][0].id].X;
                        dif.Y = fp_pos[final_piece[piece_clicked][0].id].Y - fp_pos[final_piece[piece_clicked - rowCount][0].id].Y - piece_width * 1.005f;
                        if (final_piece[piece_clicked][0].id / rowCount == 0)
                            dif.Y = fp_pos[final_piece[piece_clicked][0].id].Y - fp_pos[final_piece[piece_clicked - rowCount][0].id].Y - piece_width * 0.501f;
                        else if ((piece_clicked - rowCount) / rowCount == 0)
                            dif.Y = fp_pos[final_piece[piece_clicked][0].id].Y - fp_pos[final_piece[piece_clicked - rowCount][0].id].Y - piece_width * 0.501f;
                        fp_pos[final_piece[piece_clicked][0].id] -= dif;
                    }
                    else
                        fp_pos[final_piece[piece_clicked][i].id] -= dif;
                }

                piece1count = final_piece[piece_clicked - rowCount].Count;
                piece2count = final_piece[piece_clicked].Count;

                for (int i = 0; i < piece1count; i++)
                {
                    for (int j = 0; j < piece2count; j++)
                    {
                        if (!final_piece[final_piece[piece_clicked - rowCount][i].id].Contains(final_piece[piece_clicked][j]))
                        {
                            final_piece[final_piece[piece_clicked - rowCount][i].id].Add(final_piece[piece_clicked][j]);
                            final_piece[final_piece[piece_clicked][j].id].Add(final_piece[final_piece[piece_clicked - rowCount][i].id][0]);
                        }
                    }
                }
            }
        }

        public void CheckPieceBelow()
        {
            //Move the pieces together to a perfect fit. The user may of been close enough that we will automate them together.
            //Each piece will add each other to their list of connected pieces 
            if (!final_piece[piece_clicked].Contains(final_piece[piece_clicked + rowCount][0]))
            {
                Vector2 dif = Vector2.Zero;

                for (int i = 0; i < final_piece[piece_clicked].Count; i++)
                {
                    if (i == 0)
                    {
                        dif.X = fp_pos[final_piece[piece_clicked][0].id].X - fp_pos[final_piece[piece_clicked + rowCount][0].id].X;
                        dif.Y = fp_pos[final_piece[piece_clicked][0].id].Y - fp_pos[final_piece[piece_clicked + rowCount][0].id].Y + piece_width * 1.005f;
                        if (final_piece[piece_clicked][0].id / rowCount == 0)
                            dif.Y = fp_pos[final_piece[piece_clicked][0].id].Y - fp_pos[final_piece[piece_clicked + rowCount][0].id].Y + piece_width * 0.501f;

                        fp_pos[final_piece[piece_clicked][0].id] -= dif;
                    }
                    else
                        fp_pos[final_piece[piece_clicked][i].id] -= dif;
                }

                piece1count = final_piece[piece_clicked + rowCount].Count;
                piece2count = final_piece[piece_clicked].Count;

                for (int i = 0; i < piece1count; i++)
                {
                    for (int j = 0; j < piece2count; j++)
                    {
                        if (!final_piece[final_piece[piece_clicked + rowCount][i].id].Contains(final_piece[piece_clicked][j]))
                        {
                            final_piece[final_piece[piece_clicked + rowCount][i].id].Add(final_piece[piece_clicked][j]);
                            final_piece[final_piece[piece_clicked][j].id].Add(final_piece[final_piece[piece_clicked + rowCount][i].id][0]);
                        }
                    }
                }
            }
        }

        public void DrawPuzzlePreview(SpriteBatch _spriteBatch)
        {
            //Draw the preview image of the puzzle on the screen so the user knows what puzzle they are selecting.
            for (int i = 0; i < 8; i++)
            {
                tint = new Color(1f, 1f, 1f, 1f) * 0.24f;
                if (puzzlePick % 8 == i)
                    tint = Color.White;
                _spriteBatch.Draw(puzzlePreview[i + menuPick * 8], new Vector2(25 + 245 * (i % 4), 125 + 300 * (i / 4)), null, tint, 0.0f, Vector2.Zero, 0.45f, SpriteEffects.None, 0.0f);
                _spriteBatch.Draw(box_outlineshadow, new Vector2(25 + 245 * (i % 4) + 2, 125 + 300 * (i / 4) + 2), null, Color.White * 0.9f, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
                _spriteBatch.Draw(box_outline, new Vector2(25 + 245 * (i % 4), 125 + 300 * (i / 4)), null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
            }
        }

        public void DrawPuzzleSizeBox(SpriteBatch _spriteBatch)
        {
            if (localSettings.Values["puzzleSize"].ToString() == "layout_3")
            {
                BoxOutLined(box_16, new Vector2(125, 150), _spriteBatch);
                BoxOutLined(box_64, new Vector2(125, 400), _spriteBatch);
                BoxOutLinedSelected(box_256, new Vector2(400, 150), _spriteBatch);

            }
            else if (localSettings.Values["puzzleSize"].ToString() == "layout_1")
            {
                BoxOutLinedSelected(box_16, new Vector2(125, 150), _spriteBatch);
                BoxOutLined(box_64, new Vector2(125, 400), _spriteBatch);
                BoxOutLined(box_256, new Vector2(400, 150), _spriteBatch);
            }

            else if (localSettings.Values["puzzleSize"].ToString() == "layout_2")
            {
                BoxOutLined(box_16, new Vector2(125, 150), _spriteBatch);
                BoxOutLinedSelected(box_64, new Vector2(125, 400), _spriteBatch);
                BoxOutLined(box_256, new Vector2(400, 150), _spriteBatch);
            }

            if (localSettings.Values["mode"].ToString() == "table")
            {
                BoxOutLined(scrollmode, new Vector2(675, 150), _spriteBatch);
                BoxOutLinedSelected(tablemode, new Vector2(675, 400), _spriteBatch);
            }

            if (localSettings.Values["mode"].ToString() == "scroll")
            {
                BoxOutLinedSelected(scrollmode, new Vector2(675, 150), _spriteBatch);
                BoxOutLined(tablemode, new Vector2(675, 400), _spriteBatch);
            }
        }

        public void BoxOutLined(Texture2D boxName, Vector2 position, SpriteBatch _spriteBatch)
        {
            _spriteBatch.Draw(box_outlineshadow, position, null, Color.White * 0.9f, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
            _spriteBatch.Draw(boxName, position, null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
        }

        public void BoxOutLinedSelected(Texture2D boxName, Vector2 position, SpriteBatch _spriteBatch)
        {
            _spriteBatch.Draw(box_outlineshadow, position, null, Color.White * 0.9f, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
            _spriteBatch.Draw(box_back, position, null, Color.Green, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
            _spriteBatch.Draw(boxName, position, null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
        }


    }
}
